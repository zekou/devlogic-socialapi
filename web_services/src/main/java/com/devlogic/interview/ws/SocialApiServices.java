package com.devlogic.interview.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.devlogic.interview.io.SocialApiException;
import com.devlogic.interview.io.SocialApiResponse;


/**
 * Interface defines WS methods RESTful social API
 * @author amer
 */
@Path("/api/")
@WebService
@Produces("application/json")
public interface SocialApiServices {
	
	/**
	 * Creates user based on users Facebook token ID
	 * @param facebookToken
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@POST
	@Path("/user/create")
	public SocialApiResponse createUser(@QueryParam("fb_token") @NotNull String facebookToken);
	
	/**
	 * Stores page information and associates it with user
	 * @param pageIds
	 * @param userId
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@POST
	@Path("/pages/store")
	public SocialApiResponse createPages(@QueryParam("ids") @NotNull String pageIds, @QueryParam("user_id")  @NotNull String userId);
	
	/**
	 * Gets all pages information for the user
	 * @param userId
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@GET
	@Path("/pages/get")
	public SocialApiResponse getPagesForUser(@QueryParam("user_id") @NotNull String userId);
	
	/**
	 * Gets all posts that match query keyword
	 * @param queryKeyword
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@GET
	@Path("/search")
	public SocialApiResponse searchPostsByKeyword(@QueryParam("q") @NotNull String queryKeyword);
	
	/**
	 * Get most popular posts from all crawled pages
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@GET
	@Path("/popular")
	public SocialApiResponse searchMostPopular();
		
	/**
	 * Get most popular posts on given page
	 * @param pageId
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@GET
	@Path("/pages/popular")
	public SocialApiResponse searchPopularOnPage(@QueryParam("page_id") @NotNull String pageId);
	
	/**
	 * Get number of crawled posts for particular page
	 * @param pageId
	 * @return
	 * @throws SocialApiException
	 */
	@WebMethod
	@GET
	@Path("/count")
	public SocialApiResponse countCrawledPostsForPage(@QueryParam("page_id") @NotNull String pageId);
		
}