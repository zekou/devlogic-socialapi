package com.devlogic.interview.ws.impl;

import java.util.List;

import com.devlogic.interview.data.Page;
import com.devlogic.interview.data.Post;
import com.devlogic.interview.data.User;
import com.devlogic.interview.data.io.SocialDataRepository;
import com.devlogic.interview.io.SocialApiResponse;
import com.devlogic.interview.io.SocialApiResponse.StatusCode;
import com.devlogic.interview.ws.SocialApiServices;

public class SocialApiServicesImpl implements SocialApiServices {

	/*
	 * Business service for all API operations
	 */
	private SocialDataRepository socialDataRepo;

	@Override
	public SocialApiResponse createUser(String facebookToken) {

		SocialApiResponse response = new SocialApiResponse();

		try {
			User user = socialDataRepo.createUser(facebookToken);
			response.setMessage("User successfully added!");
			response.setStatusCode(StatusCode.OK);
			response.setUser(user);
		} catch (Exception e) {
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error!");
		}
		return response;
	}

	@Override
	public SocialApiResponse createPages(String pageIds, String userId) {
		SocialApiResponse response = new SocialApiResponse();
		try {
			Integer numUpdated = socialDataRepo.createPages(pageIds, userId);
			response.setStatusCode(StatusCode.OK);
			response.setMessage("Number of inserted pages: " + numUpdated);
		} catch (Exception e) {
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error creating pages: " + e.getMessage());
		}
		return response;
	}

	@Override
	public SocialApiResponse getPagesForUser(String userId) {
		SocialApiResponse response = new SocialApiResponse();
		try {
			List<Page> pages = socialDataRepo.getPagesForUser(userId);
			response.setStatusCode(StatusCode.OK);
			response.setMessage("Search completed!");
			response.setPages(pages);
		} catch (Exception e) {
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error getting pages: " + e.getMessage()); //TODO: Wrap in general exceptions and use CXF interceptors
		}
		return response;
	}

	@Override
	public SocialApiResponse searchPostsByKeyword(String queryKeyword) {
		SocialApiResponse response = new SocialApiResponse();
		try {
			List<Post> posts = socialDataRepo.getPostsByKeyword(queryKeyword);
			response.setStatusCode(StatusCode.OK);
			response.setMessage("Search completed!");
			response.setPosts(posts);
		} catch (Exception e){
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error getting posts: " +  e.getMessage());
		}
		return response;
	}

	@Override
	public SocialApiResponse searchMostPopular() {
		SocialApiResponse response = new SocialApiResponse();
		try{
			List<Post> posts = socialDataRepo.getMostPopularPosts();
			response.setStatusCode(StatusCode.OK);
			response.setMessage("Search completed!");
			response.setPosts(posts);
		} catch (Exception e) {
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error getting posts: " +  e.getMessage());
		}
		return response;
	}

	@Override
	public SocialApiResponse searchPopularOnPage(String pageId) {
		// TODO Auto-generated method stub
		SocialApiResponse response = new SocialApiResponse();
		try{
			List<Post> posts = socialDataRepo.getMostPopularPostsByPage(pageId);
			response.setStatusCode(StatusCode.OK);
			response.setMessage("Search completed!");
			response.setPosts(posts);
		} catch (Exception e) {
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error getting posts: " +  e.getMessage());
		}
		return response;
	}

	@Override
	public SocialApiResponse countCrawledPostsForPage(String pageId) {
		// TODO Auto-generated method stub
		SocialApiResponse response = new SocialApiResponse();
		try{
			Integer numPosts = socialDataRepo.getNumberOfPostsForPage(pageId);
			response.setStatusCode(StatusCode.OK);
			response.setMessage("Search completed!");
			response.setNumberOfPosts(numPosts);
		} catch (Exception e) {
			response.setStatusCode(StatusCode.ERROR);
			response.setMessage("Internal error while counting posts: " +  e.getMessage());
		}
		return response;
	}

	/**
	 * Set repository
	 * 
	 * @param socialDataRepo
	 */
	public void setSocialDataRepo(SocialDataRepository socialDataRepo) {
		this.socialDataRepo = socialDataRepo;
	}

}
