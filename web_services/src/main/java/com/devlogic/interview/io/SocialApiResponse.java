package com.devlogic.interview.io;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.devlogic.interview.data.Page;
import com.devlogic.interview.data.Post;
import com.devlogic.interview.data.User;

/**
 * Class used as standard API response object
 * 
 * @author amer
 * 
 */
@XmlRootElement(name = "socialapi-response")
public class SocialApiResponse {

	/**
	 * Defines status codes for response
	 * 
	 * @author amer
	 * 
	 */
	public enum StatusCode {

		OK(200), ERROR(500);

		private final Integer statusCode;

		private StatusCode(Integer status) {
			statusCode = status;
		}

		public String toString() {
			return statusCode.toString();
		}
	}

	/**
	 * Text message for additional response information
	 */
	private String message;

	/*
	 * 200-OK 
	 * 500-ERROR
	 */
	private StatusCode statusCode;

	/*
	 * User object after persistence operation
	 */
	private User user;

	/**
	 * List of pages
	 */
	private List<Page> pages;

	/**
	 * List of posts
	 */
	private List<Post> posts;
	
	/**
	 * Number of posts
	 */
	private Integer numberOfPosts;

	/**
	 * Default constructor
	 */
	public SocialApiResponse() {

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Page> getPages() {
		return pages;
	}

	public void setPages(List<Page> pages) {
		this.pages = pages;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public Integer getNumberOfPosts() {
		return numberOfPosts;
	}

	public void setNumberOfPosts(Integer numberOfPosts) {
		this.numberOfPosts = numberOfPosts;
	}
	
	

}