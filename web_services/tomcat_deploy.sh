#!/bin/bash

PROJECT_BASE=/home/amer/dev/bht-oauth-ws

#Remove existing deployment
rm -rf $CATALINA_HOME/webapps/bht-oauth-api.war
rm -rf $CATALINA_HOME/webapps/bht-oauth-api

#Copy artifact to webapp dir
cp $PROJECT_BASE/target/bht-oauth-api.war $CATALINA_HOME/webapps/bht-oauth-api.war