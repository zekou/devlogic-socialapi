package com.devlogic.interview.data.exception;

public class PageNotFoundException extends Exception{

	public PageNotFoundException() {
		
	}
	
	public PageNotFoundException(String message) {
		super(message);
	}
}
