package com.devlogic.interview.data.io;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.text.TextCriteria;
import org.springframework.data.mongodb.core.query.text.TextQuery;

import com.devlogic.interview.data.Page;
import com.devlogic.interview.data.Post;
import com.devlogic.interview.data.PostCountDTO;
import com.devlogic.interview.data.User;
import com.devlogic.interview.data.exception.PageNotFoundException;
import com.mongodb.WriteResult;

public class SocialDataRepository {
	
	/**
	 * Logger
	 */
	private Logger log = LoggerFactory.getLogger(this.getClass());
		
	/**
	 * Mongo template
	 */
	private MongoTemplate mongoTemplate;
	
	/**
	 * Saves user to datastore
	 * @param facebookToken
	 */
	public User createUser(String facebookToken) {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".createUser()"));
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(User.class)){
			mongoTemplate.createCollection(User.class);
		}
		User user = new User();
		user.setFbToken(facebookToken);
		mongoTemplate.save(user);
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".createUser()"));
		return user;
	}
	
	/**
	 * Crawls page details, saves them and associates them with user
	 * @param pageIds - CSV list of page IDs
	 * @param userId - 
	 * @return Number of successfully inserted/updated pages
	 */
	public Integer createPages(String pageIds, String userId) {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".createPages()"));	
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(Page.class)){
			mongoTemplate.createCollection(Page.class);
		}
		Set<String> pageIdSet = new HashSet<String> (Arrays.asList(pageIds.split(",")));
		Query query = null;
		Update update = null;
		int countUpdated = 0;
		for(String pageId : pageIdSet) {
			Page page = new Page();
			page.setPageId(pageId);
			
			//TODO: Invoke Facebook API to get all page details.
			//PageFacebookDetails details = new PageFacebookDetails();
			//page.setPageDetails(details);
			query = new Query(Criteria.where("pageId").is(pageId));
			update = new Update();
			//update.set("pageDetails", details);
			update.set("userId", userId);
			//Upsert by pageId
			WriteResult result = mongoTemplate.upsert(query, update, Page.class);
			countUpdated+=result.getN();
		}
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".createPages()"));	
		return countUpdated;
	}
	
	/**
	 * Returns list of pages associated to given user id
	 * @param userId - User ID
	 * @return List of pages
	 */
	public List<Page> getPagesForUser(String userId) {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".getPagesForUser()"));	
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(Page.class)){
			return new ArrayList<Page>();
		}
		Query query = new Query(Criteria.where("userId").is(userId));
		
		List<Page> pages = mongoTemplate.find(query, Page.class);
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".getPagesForUser()"));
		return pages;
	}
	
	/**
	 * Returns list of posts where post message matches<br>
	 * provided keyword. It returns only first 100 results for simplicity,
	 * but paging can be implemented easily.
	 * @param keyword - Keyword searched in posts
	 * @return List of posts
	 */
	public List<Post> getPostsByKeyword(String keyword) {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".getPostsByKeyword()"));
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(Post.class)){
			return new ArrayList<Post>();
		}
		TextCriteria criteria = TextCriteria.forDefaultLanguage();
		criteria.matching(keyword);
		Query query = TextQuery.queryText(criteria).sortByScore().with(new PageRequest(0, 100));
		List<Post> posts = mongoTemplate.find(query, Post.class);
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".getPostsByKeyword()"));				
		return posts;
	}
	
	/**
	 * Returns list of popular posts ordered by popularity score.<br>
	 * It returns only first 100 results for simplicity,
	 * but paging can be implemented easily.
	 * @return List of popular posts
	 */
	@Cacheable(value="mostPopularPosts", key="#root.method.name")
	public List<Post> getMostPopularPosts() {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".getMostPopularPosts()"));
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(Post.class)){
			return new ArrayList<Post>();
		}
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "popularityScore")).with(new PageRequest(0, 100));
		List<Post> posts = mongoTemplate.find(query, Post.class);
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".getMostPopularPosts()"));
		return posts;
	}
	
	/**
	 * Returns list of posts ordered by popularity score.<br>
	 * It returns only first 100 results for simplicity,
	 * but paging can be implemented easily.
	 * @param pageId
	 * @return List of popular posts matched by page id
	 * @throws PageNotFoundException
	 */
	@Cacheable(value="popularPostsByPage", key="#pageId")
	public List<Post> getMostPopularPostsByPage(String pageId) throws PageNotFoundException {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".popularPostsByPage()"));
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(Page.class)){
			throw new PageNotFoundException("Unable to find Page with id: " + pageId);
		}
		Query pageQuery = new Query(Criteria.where("pageId").is(pageId));
		Page page = mongoTemplate.findOne(pageQuery, Page.class);
		if(null == page) {
			throw new PageNotFoundException("Unable to find Page with id: " + pageId);
		}
		if(null == page.getPostIds() || page.getPostIds().isEmpty()) {
			return new ArrayList<Post>();
		}
		Query postsQuery = new Query(Criteria.where("id").in(page.getPostIds()));
		postsQuery.with(new Sort(Sort.Direction.DESC, "popularityScore")).with(new PageRequest(0, 100));
		List<Post> posts = mongoTemplate.find(postsQuery, Post.class);
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".getMostPopularPostsByPage()"));
		return posts;
	}
	
	/**
	 * Returns number of crawled posts for
	 * page with given page id.
	 * @param pageId - Page id
	 * @return Number of posts for this page
	 * @throws PageNotFoundException - If no page with given id is found
	 */
	public Integer getNumberOfPostsForPage(String pageId) throws PageNotFoundException {
		log.debug("Inside of ".concat(this.getClass().getSimpleName()).concat(".getNumberOfPostsForPage()"));
		//TODO: Exception handling for input params
		if(!mongoTemplate.collectionExists(Page.class)){
			throw new PageNotFoundException("Unable to find Page with id: " + pageId);
		}
		Query pageQuery = new Query(Criteria.where("pageId").is(pageId));
		Page page = mongoTemplate.findOne(pageQuery, Page.class);
		if(null == page) {
			throw new PageNotFoundException("Unable to find Page with id: " + pageId);
		}
		Aggregation agg = newAggregation(
			match(Criteria.where("_id").is(pageId)),
			unwind("postIds"),
			group("_id").count().as("nPosts"),
			project("nPosts").and("_id").as("pageId")
		);
		AggregationResults<PostCountDTO> results = mongoTemplate.aggregate(agg, "page", PostCountDTO.class);
		List<PostCountDTO> postCount = results.getMappedResults();
		if(postCount.isEmpty()) {
			return 0; //If page is matched but has no posts, result set is empty so return 0 posts
		}
		Integer nPosts = postCount.get(0).nPosts;
		log.debug("Exiting ".concat(this.getClass().getSimpleName()).concat(".getMostPopularPostsByPage()"));
		return nPosts;
	}
	
	/**
	 * Setter for Mongo Template
	 * @param mongoTemplate
	 */
	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}
	
}
