package com.devlogic.interview.data;

/**
 * DTO object used for collecting 
 * number of posts for given page.
 * Used by MongoDB aggregation operation
 * as a result transfer object.
 * @author root
 *
 */
public class PostCountDTO {
	public String pageId;
	public int nPosts;
}
