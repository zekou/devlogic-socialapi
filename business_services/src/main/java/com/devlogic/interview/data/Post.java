package com.devlogic.interview.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represents Facebook Post on specific page
 * @author amer
 */
@Document
@XmlRootElement
public class Post {

	/*
	 * Post id
	 */
	private String id;
	
	/*
	 * Post message
	 */
	@TextIndexed
	private String message;
	
	/*
	 * Number of likes of this post
	 */
	private Integer numberOfLikes;
	
	/*
	 * Number of comments on this post
	 */
	private Integer numberOfComments;
	
	/*
	 * Number of shares of this post
	 */
	private Integer numberOfShares;
	
	/*
	 * Popularity score which is calculated based on weighted sum<br>
	 * of shares, comments and likes.<br>
	 * Weights are as follows: <br>
	 * share = 10
	 * comment = 5
	 * like = 1
	 */
	private Integer popularityScore;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getNumberOfLikes() {
		return numberOfLikes;
	}

	public void setNumberOfLikes(Integer numberOfLikes) {
		this.numberOfLikes = numberOfLikes;
	}

	public Integer getNumberOfComments() {
		return numberOfComments;
	}

	public void setNumberOfComments(Integer numberOfComments) {
		this.numberOfComments = numberOfComments;
	}

	public Integer getNumberOfShares() {
		return numberOfShares;
	}

	public void setNumberOfShares(Integer numberOfShares) {
		this.numberOfShares = numberOfShares;
	}

	public Integer getPopularityScore() {
		return popularityScore;
	}

	public void setPopularityScore(Integer popularityScore) {
		this.popularityScore = popularityScore;
	}
	
}
