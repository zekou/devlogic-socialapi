package com.devlogic.interview.data;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represents Facebook Page
 * @author amer
 *
 */
@Document
@XmlRootElement(name = "page")
public class Page {
	
	/*
	 * Facebook page id
	 */
	@Id
	private String pageId;
	
	/*
	 * Details about page
	 */
	private PageFacebookDetails pageDetails;
	
	/*
	 * All posts for this page 
	 */
	private List<String> postIds;
	
	/*
	 * User id 
	 */
	private String userId;
	
	/**
	 * Get Facebook page id of this page
	 * @return
	 */
	public String getPageId() {
		return pageId;
	}

	/**
	 * Set Facebook page id of this page
	 * @param pageId
	 */
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	/**
	 * Get Post IDs for this page
	 * @return
	 */
	public List<String> getPostIds() {
		return postIds;
	}

	/**
	 * Set Post IDs for this page
	 * @param postIds
	 */
	public void setPostIds(List<String> postIds) {
		this.postIds = postIds;
	}

	/**
	 * Get id of user associated with this Facebook page
	 * @return
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Set id of user associated with this Facebook page
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Get page details
	 * @return
	 */
	public PageFacebookDetails getPageDetails() {
		return pageDetails;
	}
	
	/**
	 * Set page details
	 * @param pageDetails
	 */
	public void setPageDetails(PageFacebookDetails pageDetails) {
		this.pageDetails = pageDetails;
	}
		
}
