package com.devlogic.interview.data.io;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.devlogic.interview.data.Page;

/**
 * Test class for business service implementation
 * @author amer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class SocialDataRepositoryTest {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SocialDataRepository repository;

	/**
	 * Test that new user is created
	 */
	@Test
	public void testCreateUser() throws Exception {
		String id = repository.createUser("tata.mata").getFbToken();
		if (null == id || id.isEmpty()) {
			Assert.assertFalse("User not persisted to datastore", true);
		}
		log.info("Inserted new user with ID:" + id);
	}
	
	/**
	 * Test page is created
	 * @throws Exception
	 */
	@Test
	public void testCreatePage() throws Exception {
		MongoTemplate template = repository.getMongoTemplate();
		Page page = new Page();
		page.setUserId("amer");
		page.setPageId("213068");
		template.save(page);
		Assert.assertTrue(page.getPageId() != null && !page.getPageId().isEmpty());
	}
	
	/**
	 * Test page posts count
	 */
	@Test
	public void testPagePostsCount() throws Exception{
		MongoTemplate template = repository.getMongoTemplate();
		Page page = new Page();
		page.setPageId("2210");
		page.setUserId("azec");
		List<String> postList = new ArrayList<String>();
		postList.add("53eb1a560efbe048c7ea698d");
		postList.add("53eb1a6b0efbe048c7ea698e");
		page.setPostIds(postList);
		template.save(page);
		
		Aggregation agg = newAggregation(
			match(Criteria.where("_id").is("2210")),
			unwind("postIds"),
			group("_id").count().as("nPosts"),
			project("nPosts").and("_id").as("pageId")
		);
		
		AggregationResults<PostCount> results = template.aggregate(agg, "page", PostCount.class);
		List<PostCount> postCount = results.getMappedResults();
		Assert.assertTrue(!postCount.isEmpty());
		Assert.assertTrue(postCount.get(0).nPosts == 2);
		Assert.assertTrue(postCount.get(0).pageId.equals("2210"));
	}
	
	private class PostCount {
		String pageId;
		int nPosts;
	}
	
}