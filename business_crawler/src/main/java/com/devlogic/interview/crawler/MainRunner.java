package com.devlogic.interview.crawler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/**
 * Main runner for crawler
 * @author amer
 */
public class MainRunner {
	
	private static MongoClient mongoClient;
	
	private static ApplicationContext context;
	
	private static Logger log = LoggerFactory.getLogger(MainRunner.class);
	
	public static void main(String[] args) {
		init();
		
		if(null != mongoClient) {
			DB db = mongoClient.getDB("social-api");
			DBCollection collection = db.getCollection("page");
			DBCursor pagesCursor = collection.find();
			
			ExecutorService executor = Executors.newFixedThreadPool(10); //TODO: Externalize num of threads...
			DBObject page = null;
			while(pagesCursor.hasNext()) {
				page = pagesCursor.next();
				Runnable worker = new Crawler((String) page.get("_id"));
				executor.execute(worker);
			}
			executor.shutdown();
			while(!executor.isTerminated()){
				//Wait...
			}
			log.debug("Crawling completed...");
		}
	}
	
	private static void init(){
		
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		mongoClient = (MongoClient)context.getBean("mongoClient");
		log.info("Mongo client sucessfuly initialized!");
	}
}
