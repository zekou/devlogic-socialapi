package com.devlogic.interview.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is main worker implementing crawling logic (work with Facebook Graph API)
 * on single Facebook page.
 * 
 *
 * @author amer
 *
 */
public class Crawler implements Runnable {
	
	//TODO: Think about passing either Spring MongoTemplate bean or MongoClient to persist page back to DB 
	
	private String pageId;
	
	private static Logger log = LoggerFactory.getLogger(Crawler.class);
	
	public Crawler(String pageId) {
		this.pageId = pageId;
	}
	
	@Override
	public void run() {
		log.debug(Thread.currentThread().getName()+" start. Page ID = " + pageId);
		processPage();
		log.debug(Thread.currentThread().getName()+" end.");
	}
	
	
	private void processPage() {
		log.debug("Page processed!");
		//TODO: RestFB API, helper call...
	}

}
